# gCube D4Science Data Catalog

The "gCube Data Catalogue" or "Catalogue" is a system that provides facilities for: a) (meta)data publishing and management based on roles; b) vres' products publishing; c) making data products publicly available; d) enriching products of metadata to maximise their potential reuse and making them searchable (via title, tags etc) are based on the CKAN technology. The Catalogue application allows to show all the metadata available in the CKAN instance, as well as publish a new product, retrieve the list of organizations (i.e. Virtual Research Environments) to which the user belongs and his/her already published products. Moreover, the "Catalogue" application (via configuration) provides moderation of its content enabling the user/s with the Moderator role to approve/reject the products under review.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

**Uses**

* GWT v.2.9.0. [GWT](http://www.gwtproject.org) is licensed under [Apache License 2.0](http://www.gwtproject.org/terms.html)
* GWT-Bootstrap v.2.3.2.0. [GWT-Bootstrap](https://github.com/gwtbootstrap) is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* CKAN v.2.6.x. [CKAN](https://ckan.org/) is licensed under [Creative Commons Attribution ShareAlike 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

**Related Components**

* [Catalogue Sharing Widget](https://code-repo.d4science.org/gCubeSystem/catalogue-sharing-widget)
* [Catalogue Content Moderator Widget](https://code-repo.d4science.org/gCubeSystem/ckan-content-moderator-widget)
* [GRSF Manage Widget](https://code-repo.d4science.org/gCubeSystem/grsf-manage-widget)
* [Catalogue Metadata Publisher Widget](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget)
* [Ckan2Zenodo Publisher Widget](https://code-repo.d4science.org/gCubeSystem/ckan2zenodo-publisher-widget)

and others. You can discovery all dependencies via dependency hierarchy (e.g. use [dependency:tree](https://maven.apache.org/plugins/maven-dependency-plugin/tree-mojo.html))

## Showcase

**SoBigData Catalogue**

see at [SoBigData Catalogue](https://sobigdata.d4science.org/catalogue-sobigdata)

<img src="https://gcube.wiki.gcube-system.org/images_gcube/0/0e/Catalogue-SBG-Gateway.png" style="max-width:800px;" alt="SoBigData Catalogue" />

<br />
<br />

**Ecosystem Approach to Fisheries Catalogue**

see at [Ecosystem Approach to Fisheries Catalogue](https://i-marine.d4science.org/catalogue-imarine)

<img src="https://gcube.wiki.gcube-system.org/images_gcube/3/30/Catalogue-I-Marine-Gateway.png" style="max-width:800px;" alt="Ecosystem Approach to Fisheries Catalogue" />

## Documentation

You can find the D4Science Catalogue documentation at [GCat Background Wiki Page](https://wiki.gcube-system.org/GCat_Background)

Technical note (ITA) [D4Science_Catalogue_Service](https://gcube.wiki.gcube-system.org/images_gcube/4/4f/D4Science_Catalogue_Service.pdf)

## Change log

See the [Releases](https://code-repo.d4science.org/gCubeSystem/gcube-ckan-datacatalog/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](https://infrascience.isti.cnr.it/)
* **Costantino Perciante**

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


