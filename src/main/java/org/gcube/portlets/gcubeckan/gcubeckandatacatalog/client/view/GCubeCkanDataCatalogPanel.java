/**
 *
 */

package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.datacatalogue.utillibrary.shared.RolesCkanGroupOrOrg;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.GCubeCkanDataCatalog;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.NotifyModerationStatusEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowRevertOperationWidgetEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.resource.CkanPortletResources;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.BeanUserInOrgGroupRole;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.CkanConnectorAccessPoint;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.ManageProductResponse;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.CkanToZenodoPublisherServiceAsync;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorCheckConfigs;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorWidget;
import org.gcube.portlets.widgets.ckancontentmoderator.client.util.ModerationQueryStringUtil;
import org.gcube.portlets.widgets.ckancontentmoderator.client.util.ModerationQueryStringUtil.ModerationBuilder;
import org.gcube.portlets.widgets.ckancontentmoderator.client.util.QueryStringUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * The Class GCubeCkanDataCatalogPanel.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jun 9, 2016
 */
public class GCubeCkanDataCatalogPanel extends BaseViewTemplate {

	private CkanMetadataManagementPanel managementPanel;
	private ScrollPanel centerScrollable = new ScrollPanel();
	private CkanFramePanel ckanFramePanel;
	private CkanOrganizationsPanel ckanOrganizationsPanel;
	private CkanGroupsPanel ckanGroupsPanel;
	private Image loading = new Image(CkanPortletResources.ICONS.loading());
	private RootPanel rootPanel;
	private HandlerManager eventBus;
	private CkanConnectorAccessPoint ckanAccessPoint;
	private ManageProductResponse manageProductResponse = null;
	private ManageProductResponse moderationProductResponse = null;
	private String viewPerVREPath = null;
	private static String latestSelectedProductIdentifier;
	public static final int IFRAME_FIX_HEIGHT = 1800;
	private JSONObject obj;
	private boolean reloadCatServiceConfig = false;
	private CkanContentModeratorCheckConfigs ckanModeratorCheckConfig;

	/**
	 * Instantiates a new g cube ckan data catalog panel.
	 *
	 * @param rootPanel    the root panel
	 * @param eventManager the event manager
	 */
	public GCubeCkanDataCatalogPanel(RootPanel rootPanel, HandlerManager eventManager) {

		this.rootPanel = rootPanel;
		this.eventBus = eventManager;

		// postMessage(obj.toString(), ckanFramePanel.getFrame());
		// send message about gateway url
		obj = new JSONObject();
		String landingPageVREs = Window.Location.getProtocol() + "//" + Window.Location.getHostName() + "/explore";
		JSONString value = new JSONString(landingPageVREs);
		obj.put("explore_vres_landing_page", value);

		ckanFramePanel = new CkanFramePanel(eventBus);
		managementPanel = new CkanMetadataManagementPanel(eventBus);
		ckanOrganizationsPanel = new CkanOrganizationsPanel(this);
		ckanGroupsPanel = new CkanGroupsPanel(this);
		initPanel();
		setTopPanelVisible(true);

		// decode parameters (they could have been encoded)
		final Map<String, String> paramsMap = new HashMap<String, String>(3);
		String queryParameters = Window.Location.getQueryString();
		if (queryParameters != null && !queryParameters.isEmpty()) {
			String decoded = URL.decodeQueryString(queryParameters); // equals should be encoded too (%3D)
			String[] params = decoded.substring(decoded.indexOf("?") + 1).split("&");
			for (int i = 0; i < params.length; i++) {
				String[] queryAndValue = params[i].split("=");
				paramsMap.put(queryAndValue[0], queryAndValue[1]);
			}
			GWT.log("Extracted parameters are " + paramsMap);
		}

		String pathParameter = paramsMap.get(GCubeCkanDataCatalog.GET_PATH_PARAMETER); // Window.Location.getParameter(GCubeCkanDataCatalog.GET_PATH_PARAMETER);
		String queryParameter = paramsMap.get(GCubeCkanDataCatalog.GET_QUERY_PARAMETER);// Window.Location.getParameter(GCubeCkanDataCatalog.GET_QUERY_PARAMETER);
		String queryStringParameter = paramsMap.get(GCubeCkanDataCatalog.GET_QUERY_STRING_PARAMETER);

		if (queryStringParameter != null) {
			GWT.log("Read " + GCubeCkanDataCatalog.GET_QUERY_STRING_PARAMETER + " as: " + queryStringParameter);
			String base64DecodeQueryString = QueryStringUtil.base64DecodeQueryString(queryStringParameter);

			ModerationBuilder moderationBuilder = new ModerationQueryStringUtil()
					.toModerationBuilder(base64DecodeQueryString);
			if (moderationBuilder != null) {
				GWT.log("Moderation Builder is: " + moderationBuilder);
				CkanContentModeratorWidget ccmw = new CkanContentModeratorWidget(moderationBuilder);
				ccmw.showAsModal("Manage Items");
			}
		}

		String browserLocationURL = getBrowserLocationURL();

		GCubeCkanDataCatalog.service.getCKanConnector(browserLocationURL, pathParameter, queryParameter,
				new AsyncCallback<CkanConnectorAccessPoint>() {

					@Override
					public void onSuccess(CkanConnectorAccessPoint ckan) {

						if (ckan.isOutsideLoginOnPortal()) {

							// the portlet is outside the portal and no user is logged
							// in show only home and statistics
							managementPanel.doNotShowUserRelatedInfo();

							// set the cookie as session cookie (removed on browser close)
							Cookies.setCookie("ckan_hide_header", "true", null, ".d4science.org", "/", false);

						}

						// set the iframe url
						ckanAccessPoint = ckan;
						instanceCkanFrame(ckan.buildURI());
						GCubeCkanDataCatalogPanel.this.rootPanel.remove(loading);

						String browserLocationURL = getBrowserLocationURL();

						// check if view per organisation is enabled
						// and performing some actions in this case (e.g. removed the management
						// buttons, etc.)
						GCubeCkanDataCatalog.service.isViewPerVREEnabled(browserLocationURL,
								new AsyncCallback<String>() {

									@Override
									public void onSuccess(String result) {
										GWT.log("isViewPerVREEnabled?: " + result);
										if (result != null && !result.isEmpty()) {
											// hide all management buttons
											managementPanel.removeGenericManagementButtons();
											// set real relative path
											ckanAccessPoint.addPathInfo(result);
											// save this information
											viewPerVREPath = result;
										}
									}

									@Override
									public void onFailure(Throwable caught) {

										// ?

									}
								});

						if (!ckan.isOutsideLoginOnPortal()) {

							// MANAGE CKAN MANAGEMENT PANEL ACCORDING TO MY ROLE
							GCubeCkanDataCatalog.service.getMyRole(new AsyncCallback<RolesCkanGroupOrOrg>() {

								@Override
								public void onFailure(Throwable caught) {
									managementPanel.visibilityEditItemButton(false);
									managementPanel.visibilityDeleteItemButton(false);
									managementPanel.visibilityPublishItemButton(false, true);
								}

								@Override
								public void onSuccess(RolesCkanGroupOrOrg result) {
									GWT.log("isUserLoggedInVRE: "+ckanAccessPoint.isUserLoggedInVRE());
									GWT.log("getLoggedInScope: "+ckanAccessPoint.getLoggedInScope());

									if (ckanAccessPoint.isUserLoggedInVRE()) {

										switch (result) {
										case ADMIN: {
											managementPanel.visibilityPublishItemButton(true, true);
											managementPanel.visibilityEditItemButton(true);
											managementPanel.visibilityDeleteItemButton(true);
											managementPanel.enablePublishItemButton(true);
											//RootPanel.get(GCubeCkanDataCatalog.LOGGED_IN_DIV).add(new HTML("Logged in as "+RolesCkanGroupOrOrg.ADMIN));
											break;
										}
										case EDITOR: {
											managementPanel.visibilityPublishItemButton(true, true);
											managementPanel.visibilityEditItemButton(true);
											managementPanel.visibilityDeleteItemButton(true);
											managementPanel.enablePublishItemButton(true);
											//RootPanel.get(GCubeCkanDataCatalog.LOGGED_IN_DIV).add(new HTML("Logged in as "+RolesCkanGroupOrOrg.EDITOR));
											break;
										}
										case MEMBER: {
											managementPanel.visibilityEditItemButton(false);
											managementPanel.visibilityDeleteItemButton(false);
											// Disable the button "Publish Item" is to inform the user that he/she has
											// not the rights to publish
											managementPanel.visibilityPublishItemButton(true, true);
											managementPanel.enablePublishItemButton(false);
											//RootPanel.get(GCubeCkanDataCatalog.LOGGED_IN_DIV).add(new HTML("Logged in as "+RolesCkanGroupOrOrg.MEMBER));
											break;
										}
										default: {
											managementPanel.visibilityEditItemButton(false);
											managementPanel.visibilityDeleteItemButton(false);
											// Disable the button "Publish Item" is to inform the user that he/she has
											// not the rights to publish
											managementPanel.visibilityPublishItemButton(true, true);
											managementPanel.enablePublishItemButton(false);
											break;
										}
										}
									} else {
										managementPanel.visibilityPublishItemButton(false, true);
										managementPanel.visibilityEditItemButton(false);
										managementPanel.visibilityDeleteItemButton(false);
									}

								}
							});

							// retrieve organizations
							GCubeCkanDataCatalog.service.getCkanOrganizationsNamesAndUrlsForUser(
									new AsyncCallback<List<BeanUserInOrgGroupRole>>() {

										@Override
										public void onSuccess(List<BeanUserInOrgGroupRole> result) {
											ckanOrganizationsPanel.setOrganizations(result);
										}

										@Override
										public void onFailure(Throwable caught) {
											// an error message will be displayed
											ckanOrganizationsPanel.setOrganizations(null);
										}
									});

							// retrieve groups
							GCubeCkanDataCatalog.service.getCkanGroupsNamesAndUrlsForUser(
									new AsyncCallback<List<BeanUserInOrgGroupRole>>() {

										@Override
										public void onSuccess(List<BeanUserInOrgGroupRole> result) {
											ckanGroupsPanel.setGroups(result);
										}

										@Override
										public void onFailure(Throwable caught) {
											ckanGroupsPanel.setGroups(null);
										}
									});

							// check if the url encodes a revert operation to be performed
							if (paramsMap.containsKey(GCubeCkanDataCatalog.REVERT_QUERY_PARAM)
									&& paramsMap.get(GCubeCkanDataCatalog.REVERT_QUERY_PARAM).equals("true")) {

								eventBus.fireEvent(new ShowRevertOperationWidgetEvent(Window.Location.getHref()));

							}

							final CkanContentModeratorCheckConfigs moderatorcheckConfig = new CkanContentModeratorCheckConfigs();

							final Command whenDone = new Command() {

								@Override
								public void execute() {
									GWT.log("onConfigurationLoaded executed");
									boolean isContentModerationEnabled = false;
									boolean isModeratorRoleAssingned = false;
									boolean isExistsMyItemInModeration = false;

									try {
										isContentModerationEnabled = moderatorcheckConfig.isContentModerationEnabled();
										isModeratorRoleAssingned = moderatorcheckConfig.isModeratorRoleAssigned();
										isExistsMyItemInModeration = moderatorcheckConfig.isExistsMyItemInModeration();
									} catch (Exception e) {
										GWT.log("Command - Check configs error: " + e.getMessage());
									}

									ckanModeratorCheckConfig = moderatorcheckConfig;

									GWT.log("Moderation is enabled? " + isContentModerationEnabled);
									GWT.log("Moderator role is assigned? " + isModeratorRoleAssingned);
									GWT.log("isExistsMyItemInModeration? " + isExistsMyItemInModeration);

									if (isContentModerationEnabled) {
										managementPanel.showMessageCatalogueIsModerated(isContentModerationEnabled);
										eventManager.fireEvent(new NotifyModerationStatusEvent(isContentModerationEnabled));
									}

									// Enabling moderation if the moderation is active in the context and
									// the user has the role of MODERATOR in the context
									if (isContentModerationEnabled && isModeratorRoleAssingned) {
										GWT.log("The moderator role is assigned to user and the moderation is enabled in the context");
										managementPanel.showManageCMSProductsButton(isContentModerationEnabled);
										managementPanel.enableManageCMSProductsButton(isContentModerationEnabled);

									}

									// Enabling moderation if the moderation is active in the context and
									// the user has at least one item under moderation or already moderated
									if (isContentModerationEnabled && isExistsMyItemInModeration) {
										GWT.log("The user has at least one item moderated or under moderation, and the moderation is enabled in the context");
										managementPanel.showManageCMSProductsButton(isContentModerationEnabled);
										managementPanel.enableManageCMSProductsButton(isContentModerationEnabled);

									}
								}
							};

							try {
								moderatorcheckConfig.checkConfigs(whenDone, reloadCatServiceConfig);
								// reloadCatServiceConfig = false;
							} catch (Exception e) {
								GWT.log("Check configs error: " + e.getMessage());
							}

							/**
							 * (GRSF) Just check if it is enabled.. then we need to listen for dom events
							 * coming
							 */
							GCubeCkanDataCatalog.service
									.isManageProductEnabled(new AsyncCallback<ManageProductResponse>() {

										@Override
										public void onSuccess(ManageProductResponse manageResponse) {
											manageProductResponse = manageResponse;
											if (manageProductResponse != null) {
												managementPanel.showManageGRSFProductButton(
														manageProductResponse.isManageEnabled());
											}
										}

										@Override
										public void onFailure(Throwable caught) {
											managementPanel.showManageGRSFProductButton(false);
										}
									});

						}

					}

					@Override
					public void onFailure(Throwable caught) {

						GCubeCkanDataCatalogPanel.this.rootPanel.remove(loading);
						Window.alert("Sorry, An error occurred during contacting Gcube Ckan Data Catalogue!");
					}
				});

		Window.addResizeHandler(new ResizeHandler() {

			@Override
			public void onResize(ResizeEvent event) {

				GWT.log("onWindowResized width: " + event.getWidth() + " height: " + event.getHeight());
				updateSize();
			}
		});

		rootPanel.add(loading);
		rootPanel.add(this);
		updateSize();

		// listen for DOM messages
		listenForPostMessage();

		CkanToZenodoPublisherServiceAsync.Util.getInstance().checkZenodoEnvironment(new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				managementPanel.visibilityPublishOnZenodoButton(false);

			}

			@Override
			public void onSuccess(Boolean result) {
				GWT.log("checkZenodoEnvironment result: " + result);
				managementPanel.visibilityPublishOnZenodoButton(result);

			}
		});

	}

	/**
	 * Gets the browser location URL.
	 *
	 * @return the browser location URL
	 */
	public String getBrowserLocationURL() {

		String browserLocationURL = null;

		try {
			browserLocationURL = Window.Location.getHref();
		} catch (Exception e) {
			// silent
		}

		GWT.log("Returning browserLocationURL: " + browserLocationURL);
		return browserLocationURL;
	}

	public static String getLatestSelectedProductIdentifier() {
		return latestSelectedProductIdentifier;
	}

	/**
	 * Gets the top panel height.
	 *
	 * @return the top panel height
	 */
	public int getTopPanelHeight() {

		if (managementPanel.isVisible())
			return managementPanel.getCurrentHeight();
		return 0;
	}

	/**
	 * Sets the top panel visible.
	 *
	 * @param bool the new top panel visible
	 */
	public void setTopPanelVisible(boolean bool) {

		managementPanel.setVisible(bool);
		updateSize();
	}

//	/**
//	 * show or hide the Publish/Update/Delete buttons according to the role.
//	 *
//	 * @param show the show
//	 */
//	public void showPublishUpdateDeleteButtons(boolean show) {
//
//		managementPanel.visibilityUpdateDeleteButtons(show);
//	}

	/**
	 * Instance ckan frame.
	 *
	 * @param ckanUrlConnector the ckan url connector
	 * @return the frame
	 */
	public Frame instanceCkanFrame(String ckanUrlConnector) {

		ckanFramePanel.setVisible(true);
		ckanOrganizationsPanel.setVisible(false);
		ckanGroupsPanel.setVisible(false);
		return ckanFramePanel.instanceFrame(ckanUrlConnector, obj.toString(), ckanAccessPoint.getBaseUrl());
	}

	/**
	 * Inits the panel.
	 */
	private void initPanel() {

		setTopPanelVisible(false);
		addToTop(managementPanel);
		VerticalPanel containerIntoScrollPanel = new VerticalPanel();
		containerIntoScrollPanel.setWidth("100%");
		containerIntoScrollPanel.add(ckanFramePanel);
		containerIntoScrollPanel.add(ckanOrganizationsPanel);
		containerIntoScrollPanel.add(ckanGroupsPanel);
		centerScrollable.add(containerIntoScrollPanel);
		ckanOrganizationsPanel.setVisible(false);
		addToMiddle(centerScrollable);
	}

	/**
	 * Update window size.
	 */
	public void updateSize() {
		/*
		 * RootPanel workspace = rootPanel; int topBorder = workspace.getAbsoluteTop();
		 * GWT.log("top: "+topBorder); int footer = 30; // 85 footer is bottombar +
		 * sponsor int rootHeight = Window.getClientHeight() - topBorder - 5 - footer;
		 * int height = rootHeight - getTopPanelHeight(); if (ckanFramePanel.getFrame()
		 * != null) { int newH =managementPanel != null &&
		 * managementPanel.getCurrentHeight() > 0 ? managementPanel.getOffsetHeight() +
		 * height : height; ckanFramePanel.getFrame().setHeight(2000+"px"); }
		 */

		RootPanel workspace = this.rootPanel;
		int topBorder = workspace.getAbsoluteTop();
		int footer = 55;
		int rootHeight = Window.getClientHeight() - topBorder - 5 - footer;
		int height = rootHeight - getTopPanelHeight();
		if (this.ckanFramePanel.getFrame() != null) {
			int newH = this.managementPanel != null && this.managementPanel.getCurrentHeight() > 0
					? this.managementPanel.getOffsetHeight() + height
					: height;
			this.ckanFramePanel.getFrame().setHeight(newH + "px");
		}
		// workspace.setHeight(height+"px");
	}

	/**
	 * Print a message
	 * 
	 * @param string
	 */
	protected native void printString(String string) /*-{
		console.log(string);
	}-*/;

	/**
	 * Listen for post message.
	 */
	private final native void listenForPostMessage() /*-{
		var that = this;
		$wnd
				.addEventListener(
						"message",
						function(msg) {
							console.log("read message...");
							that.@org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.view.GCubeCkanDataCatalogPanel::onPostMessage(Ljava/lang/String;Ljava/lang/String;)(msg.data, msg.origin);
						});
	}-*/;

	/**
	 * On post message.
	 *
	 * @param data   the data
	 * @param origin the origin
	 */
	private void onPostMessage(String data, String origin) {
		printString("Read data: " + data + ", from origin: " + origin);
		printString("Ckan base url: " + ckanAccessPoint.getBaseUrl());

		// parsing data.. it is a json bean of the type
		printString("Incoming message is " + data + " from " + origin);

		if (ckanAccessPoint.getBaseUrl().indexOf(origin) >= 0) {
			// The data has been sent from your site
			// The data sent with postMessage is stored in event.data
			String height = null;
			String productId = null;
			boolean isProductKeyMissing = false;

			try {
				JSONValue parsedJSON = JSONParser.parseStrict(data);
				JSONObject object = parsedJSON.isObject();
				GWT.log("Object is " + object);
				if (object != null) {
					// Supporting Task #12286: parsing the translate values for 'dataset',
					// 'organization' and so on
					if (object.containsKey("translate")) {
						JSONObject theTranslate = (JSONObject) object.get("translate");
						GWT.log("theTranslate is " + object);
						for (String key : theTranslate.keySet()) {
							// GWT.log("theTranslate key " + key);
							String value = theTranslate.get(key).isString().stringValue();
							printString("Customizing navigation link '" + key + "' with translate: " + value);
							managementPanel.customizeLabelAccordingTranslate(key, value);
						}

					} else if (object.containsKey("height")) {

						height = object.get("height").isString().stringValue();
						if (object.containsKey("product"))
							productId = object.get("product").isString().stringValue();
						else
							isProductKeyMissing = true;
					}
				}
			} catch (Exception e) {
				GWT.log("Exception is " + e);
			}

			if (height != null)
				setIFrameHeight(height.toString());

			// show or hide the manage product button
			if (!isProductKeyMissing) {
				latestSelectedProductIdentifier = productId.toString();
				managementPanel.enableShareItemButton(productId != null && !productId.isEmpty());
				managementPanel.enableEditItemButton(productId != null && !productId.isEmpty());
				managementPanel.enableDeleteItemButton(productId != null && !productId.isEmpty());
				managementPanel.enablePublishOnZenodoButton(productId != null && !productId.isEmpty());
				managementPanel.enableManageGRSFProductButton(
						productId != null && !productId.isEmpty() && manageProductResponse.isManageEnabled());
				// managementPanel.enableManageCMSProductsButton(productId != null &&
				// !productId.isEmpty() && moderationProductResponse.isManageEnabled());
			}
		} else {
			// The data hasn't been sent from your site!
			// Be careful! Do not use it.
			return;
		}

	}

	/**
	 * Sets the i frame height.
	 *
	 * @param height the new i frame height
	 */
	private void setIFrameHeight(String height) {
		String parsedHeight = null;
		if (height == null || height.isEmpty())
			return;

		if (height.contains("px")) {
			parsedHeight = height;

			// Setting parsedHeight to null if the height is 0px
			String checkHeight = height;
			checkHeight = checkHeight.replaceAll("px", "");
			try {
				int intH = Integer.parseInt(checkHeight);
				if (intH == 0) {
					parsedHeight = null;
					printString("height is 0px so setting parsedHeight = null");
				}
			} catch (Exception e) {

			}

		} else {
			try {
				int intH = Integer.parseInt(height);
				parsedHeight = intH + " px";
			} catch (Exception e) {

			}
		}

		if (parsedHeight != null) {
			GWT.log("Setting new height for ckan iFrame: " + height);
			this.ckanFramePanel.getFrame().setHeight(height);
		}
	}

	/**
	 * Gets the base urlckan connector.
	 *
	 * @return the base urlckan connector
	 */
	public String getBaseURLCKANConnector() {

		return ckanAccessPoint.getBaseUrlWithContext();
	}

	/**
	 * Return the catalogue url (e.g. http://ckan-d-d4s.d4science.org:443/)
	 * 
	 * @return
	 */
	public String getCatalogueUrl() {

		printString("Base url for iframe is " + ckanAccessPoint.getCatalogueBaseUrl());
		return ckanAccessPoint.getCatalogueBaseUrl();
	}

	/**
	 * Gets the gcube token value to ckan connector.
	 *
	 * @return the gcube token value to ckan connector
	 */
	public String getGcubeTokenValueToCKANConnector() {

		return ckanAccessPoint.getGcubeTokenValue();
	}

	/**
	 * Gets the path info.
	 *
	 * @return the path info
	 */
	public String getPathInfo() {

		return ckanAccessPoint.getPathInfoParameter();
	}

	/**
	 * Show the organizations panel.
	 */
	public void showOrganizations() {

		ckanOrganizationsPanel.setVisible(true);
		ckanGroupsPanel.setVisible(false);
		ckanFramePanel.setVisible(false);
	}

	/**
	 * Show the groups panel.
	 */
	public void showGroups() {

		ckanGroupsPanel.setVisible(true);
		ckanOrganizationsPanel.setVisible(false);
		ckanFramePanel.setVisible(false);
	}

	/**
	 * Show management panel
	 * 
	 * @param show
	 */
	public void showManagementPanel(boolean show) {

		managementPanel.showManageGRSFProductButton(show);

	}

	/**
	 * Check if the view per vre is enabled
	 */
	public boolean isViewPerVREEnabled() {
		return viewPerVREPath != null;
	}

	public CkanContentModeratorCheckConfigs getCkanModeratorConfig() {
		return ckanModeratorCheckConfig;
	}
}
