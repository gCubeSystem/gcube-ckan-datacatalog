/**
 *
 */
package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.view;

import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ClickedCMSManageProductButtonEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.DeleteItemEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.EditMetadataEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.InsertMetadataEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.PublishOnZenodoEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShareLinkEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowDatasetsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowGroupsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowHomeEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowManageProductWidgetEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowOrganizationsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowStatisticsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowTypesEvent;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.DropdownButton;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Navbar;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineHTML;

/**
 * The Class CkanMetadataManagementPanel.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @author Costantino Perciante costantino.perciante@isti.cnr.it Jun 9, 2016
 * 
 */
public class CkanMetadataManagementPanel extends FlowPanel {

	private static final String MESSAGE_MODERATED_CATALOGUE = "Moderation Enabled";

	public static final String CATALOGUE_ITEMS_WERE_APPROVED_BY_CATALOGUE_MODERATOR_S = "Catalogue items were approved by Catalogue Moderator(s).";

	private static final String MY_PREFIX = "My ";

	private static final String MANAGE_GRSF_ITEM_TOOLTIP = "By pushing on this button, you will be able to manage the item you are viewing."
			+ " Manageable items are the GRSF ones.";

	private static final String MANAGE_CMS_ITEMS_TOOLTIP = "By pushing on this button, you will be able to manage (APPROVING or REJECTING) the item under approval."
			+ " Manageable items are not APPROVED and NOT PUBLISHED in the Catalogue.";

	private Navbar nav = new Navbar();

	// generic
	private Button home = new Button("Home");
	private Button organizations = new Button("Organizations");
	private Button groups = new Button("Groups");
	private Button items = new Button("Items");
	private Button types = new Button("Types");

	// user's own
	private InlineHTML pipe_MyInfo_start = null;
	private InlineHTML pipe_PublishItem_start = null;
	private InlineHTML pipe_SelectedItemOptions_start = null;

	private DropdownButton dropMyOptionButtons = new DropdownButton("My...");
	private NavLink myDatasets = new NavLink("My Items");
	private NavLink myOrganizations = new NavLink("My Organizations");
	private NavLink myGroups = new NavLink("My Groups");

	// statistics
	private Button statistics = new Button("Statistics");

	// other stuff
	private Button shareLink = new Button("Share Link");
	private Button uploadToZenodo = new Button("Upload to Zenodo");
	private Button insertMeta = new Button("Publish Item");
	private Button editMeta = new Button("Update Item");
	private Button deleteItem = new Button("Delete Item");
	private Button manageGRSFProduct = new Button("Manage GRSF Item"); // GRSF Manage
	private Button manageCModS = new Button("Manage Items"); // Moderation

	private Label messageModerationEnanbled = new Label(); // Moderation message

	private HandlerManager eventBus;

	/**
	 * Instantiates a new ckan metadata management panel.
	 *
	 * @param eventBus the event bus
	 */
	public CkanMetadataManagementPanel(HandlerManager eventBus) {
		this.eventBus = eventBus;
		// this.getElement().getStyle().setPaddingTop(H_OFFSET, Unit.PX);
		// this.getElement().getStyle().setPaddingBottom(H_OFFSET, Unit.PX);

		// set link style buttons
		home.setType(ButtonType.LINK);
		organizations.setType(ButtonType.LINK);
		groups.setType(ButtonType.LINK);
		items.setType(ButtonType.LINK);
		types.setType(ButtonType.LINK);

		statistics.setType(ButtonType.LINK);

		shareLink.setType(ButtonType.LINK);
		uploadToZenodo.setType(ButtonType.LINK);
		insertMeta.setType(ButtonType.LINK);
		editMeta.setType(ButtonType.LINK);
		deleteItem.setType(ButtonType.LINK);
		manageGRSFProduct.setType(ButtonType.PRIMARY);
		manageGRSFProduct.getElement().getStyle().setFloat(Float.RIGHT);

		manageCModS.setType(ButtonType.PRIMARY);
		manageCModS.getElement().getStyle().setFloat(Float.RIGHT);
		manageCModS.getElement().getStyle().setMarginRight(20, Unit.PX);

		messageModerationEnanbled.setTitle(MESSAGE_MODERATED_CATALOGUE);
		messageModerationEnanbled.getElement().setInnerHTML("<i class='icon-user'></i> " + MESSAGE_MODERATED_CATALOGUE);
		messageModerationEnanbled.addStyleName("moderation-enabled-flag");

		// set icons
		home.setIcon(IconType.HOME);
		organizations.setIcon(IconType.BUILDING);
		groups.setIcon(IconType.GROUP);
		items.setIcon(IconType.SITEMAP);
		types.setIcon(IconType.FILE_TEXT);
		shareLink.setIcon(IconType.SHARE);
		uploadToZenodo.setIcon(IconType.CIRCLE_ARROW_UP);
		myDatasets.setIcon(IconType.SITEMAP);
		myOrganizations.setIcon(IconType.BUILDING);
		myGroups.setIcon(IconType.GROUP);
		insertMeta.setIcon(IconType.FILE);
		editMeta.setIcon(IconType.EDIT_SIGN);
		deleteItem.setIcon(IconType.REMOVE_CIRCLE);
		statistics.setIcon(IconType.BAR_CHART);
		manageGRSFProduct.setIcon(IconType.CHECK_SIGN);
		manageCModS.setIcon(IconType.CHECK_SIGN);

		// disabling share and upload
		shareLink.setEnabled(false);
		uploadToZenodo.setEnabled(false);

		// hide upload to zenodo
		uploadToZenodo.setVisible(false);

		// hide publish/update/delete
		editMeta.setVisible(false);
		deleteItem.setVisible(false);
		insertMeta.setVisible(false);

		// hide manage GRSF product and Moderation
		manageGRSFProduct.setVisible(false);
		manageGRSFProduct.setEnabled(false);
		manageCModS.setVisible(false);
		manageCModS.setEnabled(false);
		messageModerationEnanbled.setVisible(false);

		// manage GRSF item info
		manageGRSFProduct.setTitle(MANAGE_GRSF_ITEM_TOOLTIP);
		manageGRSFProduct.getElement().getStyle().setFontWeight(FontWeight.BOLD);

		// manage CMS item info
		manageCModS.setTitle(MANAGE_CMS_ITEMS_TOOLTIP);
		manageCModS.getElement().getStyle().setFontWeight(FontWeight.BOLD);

		nav.add(messageModerationEnanbled);

		nav.setId("the_catalogue_nav_bar");
		nav.addStyleName("nav_bar_catalogue");
		// add to navigation bar
		nav.add(home);
		nav.add(organizations);
		nav.add(groups);
		nav.add(items);
		nav.add(types);
		nav.add(statistics);
		pipe_MyInfo_start = new InlineHTML(
				"<span style=\"font-weight:bold;vertical-alignment:middle;margin:0 5px;\">|</span>");
		pipe_MyInfo_start.setVisible(true);
		nav.add(pipe_MyInfo_start);

		dropMyOptionButtons.setBaseIcon(IconType.USER);
		dropMyOptionButtons.add(myOrganizations);
		dropMyOptionButtons.add(myGroups);
		dropMyOptionButtons.add(myDatasets);
		dropMyOptionButtons.setType(ButtonType.LINK);
		nav.add(dropMyOptionButtons);

		pipe_PublishItem_start = new InlineHTML(
				"<span style=\"font-weight:bold;vertical-alignment:middle;margin:0 5px;\">|</span>");
		pipe_PublishItem_start.setVisible(false);
		nav.add(pipe_PublishItem_start);
		nav.add(insertMeta);

		pipe_SelectedItemOptions_start = new InlineHTML(
				"<span style=\"font-weight:bold;vertical-alignment:middle;margin:0 5px;\">|</span>");
		pipe_SelectedItemOptions_start.setVisible(true);
		nav.add(pipe_SelectedItemOptions_start);

		nav.add(editMeta);
		nav.add(deleteItem);
		nav.add(shareLink);
		nav.add(uploadToZenodo);

		nav.add(manageGRSFProduct);
		nav.add(manageCModS);

		addHandlers();
		add(nav);
	}

	/**
	 * Adds the handlers.
	 */
	private void addHandlers() {

		home.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowHomeEvent());

			}
		});

		organizations.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowOrganizationsEvent(false));

			}
		});

		groups.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowGroupsEvent(false));

			}
		});

		items.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowDatasetsEvent(false));

			}
		});

		types.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowTypesEvent(false));

			}
		});

		insertMeta.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new InsertMetadataEvent());
			}
		});

		editMeta.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Edit Meta clicked");
				eventBus.fireEvent(
						new EditMetadataEvent(GCubeCkanDataCatalogPanel.getLatestSelectedProductIdentifier()));
			}
		});

		deleteItem.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Delete item clicked");
				eventBus.fireEvent(new DeleteItemEvent(GCubeCkanDataCatalogPanel.getLatestSelectedProductIdentifier()));
			}
		});

		myDatasets.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowDatasetsEvent(true));

			}
		});

		myOrganizations.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowOrganizationsEvent(true));

			}
		});

		myGroups.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowGroupsEvent(true));

			}
		});

		statistics.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowStatisticsEvent());

			}
		});

		manageGRSFProduct.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShowManageProductWidgetEvent(
						GCubeCkanDataCatalogPanel.getLatestSelectedProductIdentifier()));

			}
		});

		manageCModS.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ClickedCMSManageProductButtonEvent());

			}
		});

		shareLink.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(new ShareLinkEvent(GCubeCkanDataCatalogPanel.getLatestSelectedProductIdentifier()));

			}
		});

		uploadToZenodo.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				eventBus.fireEvent(
						new PublishOnZenodoEvent(GCubeCkanDataCatalogPanel.getLatestSelectedProductIdentifier()));
			}
		});
	}

	/**
	 * Gets the current height.
	 *
	 * @return the current height
	 */
	public int getCurrentHeight() {
		return this.getOffsetHeight();
	}

	/**
	 * Enable publish button.
	 *
	 * @param enable the enable
	 */
	public void enablePublishItemButton(boolean enable) {
		insertMeta.setEnabled(enable);
	}

	/**
	 * Button to manage the GRSF product.. for example in grsf case
	 *
	 * @param value true or false
	 */
	public void showManageGRSFProductButton(boolean value) {
		manageGRSFProduct.setVisible(value);
	}

	/**
	 * Enable or disable the manage product button.
	 *
	 * @param value the value
	 */
	public void enableManageGRSFProductButton(boolean value) {
		manageGRSFProduct.setEnabled(value);
	}

	/**
	 * Button to manage the products under moderation via Content Moderation System
	 * (CMS).
	 *
	 * @param value true or false
	 */
	public void showManageCMSProductsButton(boolean value) {
		manageCModS.setVisible(value);
	}

	/**
	 * Show message catalogue is moderated.
	 *
	 * @param isContentModerationEnabled the is content moderation enabled
	 */
	public void showMessageCatalogueIsModerated(boolean isContentModerationEnabled) {
		messageModerationEnanbled.setVisible(isContentModerationEnabled);

	}

	/**
	 * Enable manage CMS products button.
	 *
	 * @param value the value
	 */
	public void enableManageCMSProductsButton(boolean value) {
		manageCModS.setEnabled(value);
	}

	/**
	 * Enable or disable the share link button.
	 *
	 * @param value the value
	 */
	public void enableShareItemButton(boolean value) {
		shareLink.setEnabled(value);
	}

	/**
	 * Enable publish on zenodo button.
	 *
	 * @param value the value
	 */
	public void enablePublishOnZenodoButton(boolean value) {
		uploadToZenodo.setEnabled(value);
	}

	/**
	 * Enable edit item button.
	 *
	 * @param value the value
	 */
	public void enableEditItemButton(boolean value) {
		editMeta.setEnabled(value);
	}

	/**
	 * Enable delete item button.
	 *
	 * @param value the value
	 */
	public void enableDeleteItemButton(boolean value) {
		deleteItem.setEnabled(value);
	}

	/**
	 * Visibility publish on zenodo button.
	 *
	 * @param value the value
	 */
	public void visibilityPublishOnZenodoButton(boolean value) {
		uploadToZenodo.setVisible(value);
	}

	/**
	 * Visibility publish item button.
	 *
	 * @param value       the value
	 * @param alsoThePipe the also the pipe. If true set bool value also the related
	 *                    pipe 'pipe_PublishItem_start'
	 */
	public void visibilityPublishItemButton(boolean bool, boolean alsoThePipe) {
		insertMeta.setVisible(bool);
		if (alsoThePipe) {
			pipe_PublishItem_start.setVisible(bool);
		}
	}

	/**
	 * Visibility edit item button.
	 *
	 * @param value the value
	 */
	public void visibilityEditItemButton(boolean value) {
		editMeta.setVisible(value);
	}

	/**
	 * Visibility delete item button.
	 *
	 * @param value the value
	 */
	public void visibilityDeleteItemButton(boolean value) {
		deleteItem.setVisible(value);
	}

	/**
	 * Show only home/statistics buttons.
	 */
	public void doNotShowUserRelatedInfo() {

		pipe_MyInfo_start.setVisible(false);
		pipe_PublishItem_start.setVisible(false);
		pipe_SelectedItemOptions_start.setVisible(false);
		shareLink.setVisible(false);
		uploadToZenodo.setVisible(false);
		insertMeta.setVisible(false);
		editMeta.setVisible(false);
		deleteItem.setVisible(false);
		dropMyOptionButtons.setVisible(false);
//		myDatasets.setVisible(false);
//		myOrganizations.setVisible(false);
//		myGroups.setVisible(false);
		manageGRSFProduct.setVisible(false);
		manageCModS.setVisible(false);

	}

	/**
	 * Removes the generic management buttons.
	 */
	public void removeGenericManagementButtons() {

		home.setVisible(true);
		organizations.setVisible(false);
		groups.setVisible(false);
		items.setVisible(false);
		types.setVisible(false);
		pipe_MyInfo_start.setVisible(false);
		pipe_PublishItem_start.setVisible(false);
		dropMyOptionButtons.setVisible(false);
//		myDatasets.setVisible(false);
//		myOrganizations.setVisible(false);
//		myGroups.setVisible(false);
		statistics.setVisible(false);
		manageGRSFProduct.setVisible(false);
		manageCModS.setVisible(false);

	}

	/**
	 * Customize label according translate.
	 *
	 * @param labelName      the label name
	 * @param translateValue the translate value
	 */
	public void customizeLabelAccordingTranslate(String labelName, String translateValue) {

		if (labelName == null || labelName.isEmpty() || translateValue == null || translateValue.isEmpty())
			return;

//		GWT.log("labelName "+labelName);
//		GWT.log("translateValue "+translateValue);
//		GWT.log("organizations "+organizations.getText());

		if (labelName.compareToIgnoreCase(organizations.getText().trim()) == 0) {
			organizations.setText(translateValue);
			// return;
		} else if (labelName.compareToIgnoreCase(groups.getText().trim()) == 0) {
			groups.setText(translateValue);
			// return;
		} else if (labelName.compareToIgnoreCase(items.getText().trim()) == 0) {
			items.setText(translateValue);
			// return;
		} else if (labelName.compareToIgnoreCase(types.getText().trim()) == 0) {
			types.setText(translateValue);
			// return;
		}

		String mylabelName = MY_PREFIX + labelName;
//		GWT.log("mylabelName "+mylabelName);
//		GWT.log("myDatasets.getText() "+myDatasets.getText());

		if (mylabelName.compareToIgnoreCase(myDatasets.getText().trim()) == 0) {
			myDatasets.setText(MY_PREFIX + translateValue);
			// return;
		} else if (mylabelName.compareToIgnoreCase(myOrganizations.getText().trim()) == 0) {
			myOrganizations.setText(MY_PREFIX + translateValue);
			// return;
		} else if (mylabelName.compareToIgnoreCase(myGroups.getText().trim()) == 0) {
			myGroups.setText(MY_PREFIX + translateValue);
			// return;
		}

	}

	/**
	 * Capitalize.
	 *
	 * @param stringValue the translate value
	 * @return the string
	 */
	public static String capitalize(String stringValue) {
		return stringValue.substring(0, 1).toUpperCase() + stringValue.substring(1, stringValue.length());
	}

}
