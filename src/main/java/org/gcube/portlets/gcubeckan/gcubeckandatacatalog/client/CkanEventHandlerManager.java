
package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client;

import org.gcube.datacatalogue.grsf_manage_widget.client.events.HideManagementPanelEvent;
import org.gcube.datacatalogue.grsf_manage_widget.client.events.HideManagementPanelEventHandler;
import org.gcube.datacatalogue.grsf_manage_widget.client.view.ManageProductWidget;
import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ClickedCMSManageProductButtonEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ClickedCMSManageProductButtonEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.DeleteItemEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.DeleteItemEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.EditMetadataEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.EditMetadataEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.InsertMetadataEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.InsertMetadataEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.NotifyModerationStatusEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.NotifyModerationStatusEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.PublishOnZenodoEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.PublishOnZenodoEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShareLinkEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShareLinkEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowDatasetsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowDatasetsEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowGroupsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowGroupsEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowHomeEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowHomeEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowManageProductWidgetEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowManageProductWidgetEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowOrganizationsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowOrganizationsEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowRevertOperationWidgetEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowRevertOperationWidgetEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowStatisticsEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowStatisticsEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowTypesEvent;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event.ShowTypesEventHandler;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.view.GCubeCkanDataCatalogPanel;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.CkanConnectorAccessPoint;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.CkanToZendoPublisherWidget;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.CatalogueItem;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorCheckConfigs;
import org.gcube.portlets.widgets.ckancontentmoderator.client.CkanContentModeratorWidgetTrusted;
import org.gcube.portlets.widgets.ckancontentmoderator.client.ContentModeratorWidgetConstants;
import org.gcube.portlets.widgets.ckancontentmoderator.shared.DISPLAY_FIELD;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.action.DeleteItemPanel;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.form.CreateDatasetForm;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.form.UpdateDatasetForm;
import org.gcube.portlets_widgets.catalogue_sharing_widget.client.ShareCatalogueWidget;

import com.github.gwtbootstrap.client.ui.Modal;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class CkanEventHandlerManager.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @author Costantino Perciante costantino.perciante@isti.cnr.it Jun 10, 2016
 */
public class CkanEventHandlerManager {

	protected static final String WIDGET_CREATE_TITLE = "Publish Item";
	private HandlerManager eventBus = new HandlerManager(null);
	private GCubeCkanDataCatalogPanel gcubeCkanDataCatalogue;
	private String logoutFromCKANURL;

	// Feature #28746
	private Boolean catalogueModerated = false;

	/**
	 * Instantiates a new ckan event handler manager.
	 */
	public CkanEventHandlerManager() {
		bind();
	}

	/**
	 * Sets the gcubeCkanDataCatalogue.
	 *
	 * @param gcubeCkanDataCatalogue the gcubeCkanDataCatalogue to set
	 */
	public void setPanel(GCubeCkanDataCatalogPanel panel) {

		this.gcubeCkanDataCatalogue = panel;
	}

	/**
	 * Bind.
	 */
	private void bind() {

		// bind on show home event
		eventBus.addHandler(ShowHomeEvent.TYPE, new ShowHomeEventHandler() {

			@Override
			public void onShowHome(ShowHomeEvent showHomeEvent) {
				String pathToUse = gcubeCkanDataCatalogue.isViewPerVREEnabled() ? gcubeCkanDataCatalogue.getPathInfo()
						: null;
				String request = getCkanRequest(pathToUse, null);
				gcubeCkanDataCatalogue.instanceCkanFrame(request);

			}
		});

		// bind on insert metadata
		eventBus.addHandler(InsertMetadataEvent.TYPE, new InsertMetadataEventHandler() {

			@Override
			public void onInsertMetadata(InsertMetadataEvent loadSelectedItemEvent) {
				GWT.log("onInsertMetadata: " + loadSelectedItemEvent);
				final Modal modal = new ExtModal(true, true);

				modal.setTitle(WIDGET_CREATE_TITLE);
				modal.addStyleName("insert-metadata-modal-style");
				modal.addStyleName("modal-top-custom");
				((Element) modal.getElement().getChildNodes().getItem(1)).addClassName("modal-body-custom");
				CreateDatasetForm cdf = new CreateDatasetForm(eventBus);
				// Feature #28746
				cdf.setCatalogueModerated(catalogueModerated);
				modal.add(cdf);
				modal.setCloseVisible(true);

//				// hide any popup gcubeCkanDataCatalogue opened
//				modal.addHideHandler(new HideHandler() {
//
//					@Override
//					public void onHide(HideEvent hideEvent) {
//						GWT.log("HideEvent on modal fired");
//						GWT.log(hideEvent.toDebugString());
//						eventBus.fireEvent(new CloseCreationFormProfileEvent());
//						
//						hideEvent.preventDefault();
//						hideEvent.stopPropagation();
//						return;
//						
//					}
//				});

				GWT.log("show");
				modal.show();
			}
		});

		// show datasets event
		eventBus.addHandler(ShowDatasetsEvent.TYPE, new ShowDatasetsEventHandler() {

			@Override
			public void onShowDatasets(ShowDatasetsEvent showUserDatasetsEvent) {
				String request = null;

				if (showUserDatasetsEvent.getDatasetId() == null) {
					if (showUserDatasetsEvent.isOwnOnly()) {
						request = getCkanRequest("/dashboard/datasets", null);
					} else {
						request = getCkanRequest("/dataset", null);
					}
				} else {
					String datasetId = showUserDatasetsEvent.getDatasetId();
					request = getCkanRequest("/dataset/" + datasetId, null);

				}
				gcubeCkanDataCatalogue.instanceCkanFrame(request);
			}
		});

		// ReloadDatasetPageEvent is declared in the Publisher Widget
		eventBus.addHandler(ReloadDatasetPageEvent.TYPE, new ReloadDatasetPageEventHandler() {

			@Override
			public void onReloadDatasetPage(ReloadDatasetPageEvent addResourceEvent) {

				if (addResourceEvent.getDatasetIDorName() != null) {
					eventBus.fireEvent(new ShowDatasetsEvent(false, addResourceEvent.getDatasetIDorName()));
				} else {
					eventBus.fireEvent(new ShowDatasetsEvent(false));
				}

			}
		});

		eventBus.addHandler(ShowTypesEvent.TYPE, new ShowTypesEventHandler() {

			@Override
			public void onShowTypes(ShowTypesEvent showTypes) {
				String request = getCkanRequest("/type", null);
				gcubeCkanDataCatalogue.instanceCkanFrame(request);

			}
		});

		eventBus.addHandler(ShowOrganizationsEvent.TYPE, new ShowOrganizationsEventHandler() {

			@Override
			public void onShowOrganizations(ShowOrganizationsEvent showUserDatasetsEvent) {

				if (showUserDatasetsEvent.isOwnOnly())
					gcubeCkanDataCatalogue.showOrganizations();
				else {
					String request = getCkanRequest("/organization", null);
					gcubeCkanDataCatalogue.instanceCkanFrame(request);
				}
			}
		});
		eventBus.addHandler(ShowGroupsEvent.TYPE, new ShowGroupsEventHandler() {

			@Override
			public void onShowGroups(ShowGroupsEvent showGroupsEvent) {
				// gcubeCkanDataCatalogue.instanceCkanFrame(request);
				if (showGroupsEvent.isOwnOnly())
					gcubeCkanDataCatalogue.showGroups();
				else {
					String request = getCkanRequest("/group", null);
					gcubeCkanDataCatalogue.instanceCkanFrame(request);
				}
			}
		});

		// show statistics event
		eventBus.addHandler(ShowStatisticsEvent.TYPE, new ShowStatisticsEventHandler() {

			@Override
			public void onShowStatistics(ShowStatisticsEvent showStatisticsEvent) {
				String request = getCkanRequest("/stats", null);
				gcubeCkanDataCatalogue.instanceCkanFrame(request);
			}
		});

		eventBus.addHandler(ShowManageProductWidgetEvent.TYPE, new ShowManageProductWidgetEventHandler() {

			@Override
			public void onShowManageProductWidget(ShowManageProductWidgetEvent event) {
				new ManageProductWidget(event.getProductIdentifier(), eventBus);
			}
		});

		eventBus.addHandler(ClickedCMSManageProductButtonEvent.TYPE, new ClickedCMSManageProductButtonEventHandler() {

			@Override
			public void onClickedManageProduct(ClickedCMSManageProductButtonEvent showManageProductWidgetEvent) {

				CkanContentModeratorCheckConfigs config = gcubeCkanDataCatalogue.getCkanModeratorConfig();

				DISPLAY_FIELD[] sortByFields = null;

				try {
					if (config.isModeratorRoleAssigned()) {
						sortByFields = DISPLAY_FIELD.values();
					}
				} catch (Exception e) {
				}

				if (sortByFields == null) {
					sortByFields = ContentModeratorWidgetConstants.DEFAULT_SORT_BY_FIELDS;
				}

				final CkanContentModeratorWidgetTrusted cmsTrusted = new CkanContentModeratorWidgetTrusted(
						ItemStatus.PENDING, DISPLAY_FIELD.values(), sortByFields, config);

				cmsTrusted.showAsModal(null);
			}
		});

		eventBus.addHandler(ShowRevertOperationWidgetEvent.TYPE, new ShowRevertOperationWidgetEventHandler() {
			@Override
			public void onShowRevertOperationWidgetEvent(ShowRevertOperationWidgetEvent event) {

				new ManageProductWidget(event.getEncryptedUrl(), eventBus);

			}
		});

		eventBus.addHandler(ShareLinkEvent.TYPE, new ShareLinkEventHandler() {

			@Override
			public void onShareLink(ShareLinkEvent event) {

				new ShareCatalogueWidget(event.getUuidItem());

			}
		});

		// hide management gcubeCkanDataCatalogue if user is not allowed to manage
		eventBus.addHandler(HideManagementPanelEvent.TYPE, new HideManagementPanelEventHandler() {

			@Override
			public void onEvent(HideManagementPanelEvent hideEvent) {

				gcubeCkanDataCatalogue.showManagementPanel(false);

			}
		});

		eventBus.addHandler(PublishOnZenodoEvent.TYPE, new PublishOnZenodoEventHandler() {

			@Override
			public void onPublishOnZenodo(PublishOnZenodoEvent publishOnZenodoEvent) {

				if (publishOnZenodoEvent.getItemId() != null) {

					CkanToZendoPublisherWidget publisherWidget = new CkanToZendoPublisherWidget();
					CatalogueItem ci = new CatalogueItem(publishOnZenodoEvent.getItemId(), null, null,
							CatalogueItem.ITEM_TYPE.DATASET);
					publisherWidget.publishOnZenodo(ci);

				}
			}

		});

		eventBus.addHandler(EditMetadataEvent.TYPE, new EditMetadataEventHandler() {

			@Override
			public void onEditMetadata(EditMetadataEvent editMetadataEvent) {
				GWT.log("editMetadataEvent: " + editMetadataEvent);

				if (editMetadataEvent.getItemID() != null) {

					final Modal modal = new ExtModal(true, true);

					modal.setTitle("Update Item");
					modal.addStyleName("insert-metadata-modal-style");
					modal.addStyleName("modal-top-custom");
					((Element) modal.getElement().getChildNodes().getItem(1)).addClassName("modal-body-custom");
					UpdateDatasetForm udf = new UpdateDatasetForm(eventBus, editMetadataEvent.getItemID());
					// Feature #28746
					udf.setCatalogueModerated(catalogueModerated);
					modal.add(udf);
					modal.setCloseVisible(true);

					GWT.log("show");
					modal.show();

				}
			}
		});

		eventBus.addHandler(DeleteItemEvent.TYPE, new DeleteItemEventHandler() {

			@Override
			public void onDeleteItem(DeleteItemEvent deleteItemEvent) {
				GWT.log("deleteItemEvent: " + deleteItemEvent);

				if (deleteItemEvent.getItemID() != null) {

					GWT.log("deleteItemEvent: " + deleteItemEvent);
					final Modal modal = new ExtModal(true, true);

					modal.setTitle("Delete Item");
					modal.addStyleName("modal-top-custom");
					((Element) modal.getElement().getChildNodes().getItem(1)).addClassName("modal-body-custom");
					modal.add(new DeleteItemPanel(eventBus, deleteItemEvent.getItemID()));
					modal.setCloseVisible(true);

					GWT.log("show");
					modal.show();

				}

			}
		});

		eventBus.addHandler(NotifyModerationStatusEvent.TYPE, new NotifyModerationStatusEventHandler() {

			@Override
			public void onStatus(NotifyModerationStatusEvent notifyModerationStatusEvent) {

				catalogueModerated = notifyModerationStatusEvent.isModerationEnabled();
				GWT.log("Is catalogue Moderated? " + catalogueModerated);

			}
		});

	}

	/**
	 * Gets the ckan request.
	 *
	 * @param pathInfo the path info
	 * @param query    the query
	 * @return the ckan request
	 */
	private String getCkanRequest(String pathInfo, String query) {
		CkanConnectorAccessPoint ckan = new CkanConnectorAccessPoint(gcubeCkanDataCatalogue.getBaseURLCKANConnector(),
				"");
		if (gcubeCkanDataCatalogue.getGcubeTokenValueToCKANConnector() != null)
			ckan.addGubeToken(gcubeCkanDataCatalogue.getGcubeTokenValueToCKANConnector());
		pathInfo = CkanConnectorAccessPoint.checkNullString(pathInfo);
		query = CkanConnectorAccessPoint.checkNullString(query);
		ckan.addPathInfo(pathInfo);
		ckan.addQueryString(query);
		return ckan.buildURI();
	}

	/**
	 * Instance logout system.
	 */
	private void instanceLogoutSystem() {

		GCubeCkanDataCatalog.service.logoutFromCkanURL(new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				logoutFromCKANURL = result;
				GWT.log("Loaded logout url: " + logoutFromCKANURL);
				performLogoutOnBrowserClosedEvent(logoutFromCKANURL);
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

	}

	/**
	 * Perform logout on browser closed event.
	 *
	 * @param logoutService the logout service
	 */
	private static native void performLogoutOnBrowserClosedEvent(String logoutService)/*-{

		var validNavigation = false;

		function wireUpEvents() {
			console.log("wireUpEvents");
			var dont_confirm_leave = 1; //set dont_confirm_leave to 1 when you want the user to be able to leave without confirmation
			var leave_message = 'You sure you want to leave?'

			function disconnect(e) {

				if (!validNavigation) {
					var logoutPerformed = false;

					var ifrm = $doc.createElement("iframe");
					ifrm.id = 'logout-iframe';
					ifrm.onload = function() {
						logoutPerformed = true;
						console.log("ifrm loaded exit is: " + exit);
					}
					ifrm.style.width = "1px";
					ifrm.style.height = "1px";
					ifrm.src = logoutService;
					$doc.body.appendChild(ifrm);

					//sleep 500ms in order to loasad disconnect response performed by IFrame
					function sleep(milliseconds) {
						var start = new Date().getTime();
						for (var i = 0; i < 1e7; i++) {
							if ((new Date().getTime() - start) > milliseconds
									|| logoutPerformed) {
								break;
							}
						}
					}
					//sleep 500ms in order to have time to load disconnect response returned by IFrame
					sleep(500);

					if (dont_confirm_leave !== 1) {
						if (!e)
							e = window.event;
						//e.cancelBubble is supported by IE - this will kill the bubbling process.
						e.cancelBubble = true;
						e.returnValue = leave_message;
						//e.stopPropagation works in Firefox.
						if (e.stopPropagation) {
							e.stopPropagation();
							e.preventDefault();
						}
						//return works for Chrome and Safari
						return leave_message;
					}
				}
			}

			window.onbeforeunload = disconnect;

			// Attach the event keypress to exclude the F5 refresh
			$wnd.$(document).bind('keypress', function(e) {
				if (e.keyCode == 116) {
					validNavigation = true;
					console.log("keypress: " + validNavigation);
				}
			});

			// Attach the event click for all links in the page
			$wnd.$("a").bind("click", function() {
				validNavigation = true;
				console.log("click: " + validNavigation);
			});

			// Attach the event submit for all forms in the page
			$wnd.$("form").bind("submit", function() {
				validNavigation = true;
				console.log("form: " + validNavigation);
			});

			// Attach the event click for all inputs in the page
			$wnd.$("input[type=submit]").bind("click", function() {
				validNavigation = true;
				console.log("submit: " + validNavigation);
			});

		}

		// Wire up the events as soon as the DOM tree is ready
		$wnd.$(document).ready(function() {
			wireUpEvents();
		});

		//wireUpEvents();

	}-*/;

	/**
	 * Gets the event bus.
	 *
	 * @return the event bus
	 */
	public HandlerManager getEventBus() {

		return eventBus;
	}

	/**
	 * Gets the logout from ckanurl.
	 *
	 * @return the logoutFromCKANURL
	 */
	public String getLogoutFromCKANURL() {

		return logoutFromCKANURL;
	}
}
