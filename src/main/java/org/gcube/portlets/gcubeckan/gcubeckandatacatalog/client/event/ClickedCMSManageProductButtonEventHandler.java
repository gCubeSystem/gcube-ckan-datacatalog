package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ClickedCMSManageProductButtonEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 15, 2021
 */
public interface ClickedCMSManageProductButtonEventHandler extends EventHandler {

	/**
	 * On clicked manage product.
	 *
	 * @param showManageProductWidgetEvent the show manage product widget event
	 */
	void onClickedManageProduct(ClickedCMSManageProductButtonEvent showManageProductWidgetEvent);

}
