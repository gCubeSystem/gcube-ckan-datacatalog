package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface PublishOnZenodoEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 9, 2019
 */
public interface PublishOnZenodoEventHandler extends EventHandler  {

	void onPublishOnZenodo(PublishOnZenodoEvent publishOnZenodoEvent);
}
