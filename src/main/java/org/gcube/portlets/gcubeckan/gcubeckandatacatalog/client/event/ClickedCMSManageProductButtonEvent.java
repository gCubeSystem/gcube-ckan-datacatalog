package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class ClickedCMSManageProductButtonEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 15, 2021
 */
public class ClickedCMSManageProductButtonEvent extends GwtEvent<ClickedCMSManageProductButtonEventHandler> {

	public static Type<ClickedCMSManageProductButtonEventHandler> TYPE = new Type<ClickedCMSManageProductButtonEventHandler>();

	/**
	 * Instantiates a new clicked Content Moderation System (CMS) manage product button event.
	 */
	public ClickedCMSManageProductButtonEvent() {
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ClickedCMSManageProductButtonEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ClickedCMSManageProductButtonEventHandler handler) {
		handler.onClickedManageProduct(this);
	}

}
