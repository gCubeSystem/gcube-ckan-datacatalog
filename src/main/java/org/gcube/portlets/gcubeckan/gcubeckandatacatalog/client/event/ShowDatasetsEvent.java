package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ShowDatasetsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 13, 2024
 */
public class ShowDatasetsEvent extends GwtEvent<ShowDatasetsEventHandler> {

	public static Type<ShowDatasetsEventHandler> TYPE = new Type<ShowDatasetsEventHandler>();

	private boolean ownOnly;

	private String datasetId;

	/**
	 * Instantiates a new show user datasets event.
	 *
	 * @param ownOnly the own only
	 */
	public ShowDatasetsEvent(boolean ownOnly) {
		this.ownOnly = ownOnly;
	}

	/**
	 * Instantiates a new show user datasets event.
	 *
	 * @param ownOnly   the own only
	 * @param datasetId the dataset id
	 */
	public ShowDatasetsEvent(boolean ownOnly, String datasetId) {
		this.ownOnly = ownOnly;
		this.datasetId = datasetId;
	}

	/**
	 * Checks if is own only.
	 *
	 * @return true, if is own only
	 */
	public boolean isOwnOnly() {
		return ownOnly;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ShowDatasetsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(ShowDatasetsEventHandler handler) {
		handler.onShowDatasets(this);
	}
	
	public String getDatasetId() {
		return datasetId;
	}

}
