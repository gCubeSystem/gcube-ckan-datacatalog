/**
 *
 */
package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client;

import java.util.List;

import org.gcube.datacatalogue.utillibrary.shared.RolesCkanGroupOrOrg;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.BeanUserInOrgGroupRole;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.CkanConnectorAccessPoint;
import org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared.ManageProductResponse;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Interface GcubeCkanDataCatalogServiceAsync.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Nov 4, 2016
 */
public interface GcubeCkanDataCatalogServiceAsync {

	/**
	 * Gets the my role.
	 *
	 * @param callback the callback
	 * @return the my role
	 */
	void getMyRole(AsyncCallback<RolesCkanGroupOrOrg> callback);

	/**
	 * Gets the CKAN connector.
	 *
	 * @param browserLocationURL    the browser location URL that performs the call
	 * @param pathInfoParameters    the path info parameters
	 * @param queryStringParameters the query string parameters
	 * @param callback              the callback
	 * @return the c kan connector
	 */
	void getCKanConnector(String browserLocationURL, String pathInfoParameters, String queryStringParameters,
			AsyncCallback<CkanConnectorAccessPoint> callback);

	/**
	 * Logout from ckan url.
	 *
	 * @param callback the callback
	 */
	void logoutFromCkanURL(AsyncCallback<String> callback);

	/**
	 * Gets the ckan organizations names and urls for user.
	 *
	 * @param callback the callback
	 * @return the ckan organizations names and urls for user
	 */
	void getCkanOrganizationsNamesAndUrlsForUser(AsyncCallback<List<BeanUserInOrgGroupRole>> callback);

	/**
	 * Retrieve the list of groups to whom the user belongs and their urls.
	 *
	 * @param callback the callback
	 * @return the ckan groups names and urls for user
	 */
	void getCkanGroupsNamesAndUrlsForUser(AsyncCallback<List<BeanUserInOrgGroupRole>> callback);

	/**
	 * Checks if is manage product enabled.
	 *
	 * @param callback the callback
	 */
	void isManageProductEnabled(AsyncCallback<ManageProductResponse> callback);

	/**
	 * Checks if is view per VRE enabled.
	 *
	 * @param browserLocationURL the browser location URL
	 * @param callback           the callback
	 */
	void isViewPerVREEnabled(String browserLocationURL, AsyncCallback<String> callback);

	/**
	 * Outside login portal.
	 *
	 * @param callback the callback
	 */
	void outsideLoginPortal(AsyncCallback<Boolean> callback);

	/**
	 * Check moderation enabled.
	 *
	 * @param reloadConfig the reload config
	 * @param callback     the callback
	 */
	void checkModerationEnabled(boolean reloadConfig, AsyncCallback<ManageProductResponse> callback);
}
