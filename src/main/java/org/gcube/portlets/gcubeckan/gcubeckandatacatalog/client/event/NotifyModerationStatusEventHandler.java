package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface NotifyModerationStatusEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 5, 2025
 */
public interface NotifyModerationStatusEventHandler extends EventHandler {

	/**
	 * On status.
	 *
	 * @param notifyModerationStatusEvent the notify moderation status event
	 */
	void onStatus(NotifyModerationStatusEvent notifyModerationStatusEvent);
}
