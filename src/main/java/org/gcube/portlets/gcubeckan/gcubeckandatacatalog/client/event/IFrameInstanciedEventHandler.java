package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface IFrameInstanciedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 13, 2024
 */
public interface IFrameInstanciedEventHandler extends EventHandler {

	/**
	 * On new instance.
	 *
	 * @param iFrameInstanciedEent the i frame instancied eent
	 */
	void onNewInstance(IFrameInstanciedEvent iFrameInstanciedEent);

}