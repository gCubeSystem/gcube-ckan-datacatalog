package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class PublishOnZenodoEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 9, 2019
 */
public class PublishOnZenodoEvent extends GwtEvent<PublishOnZenodoEventHandler>{
	
	public static Type<PublishOnZenodoEventHandler> TYPE = new Type<PublishOnZenodoEventHandler>();
	private String itemId;

	
	/**
	 * Instantiates a new publish on zenodo event.
	 *
	 * @param itemId the item id
	 */
	public PublishOnZenodoEvent(String itemId) {
		this.itemId = itemId;
	}
	
	public String getItemId() {
		return itemId;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<PublishOnZenodoEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(PublishOnZenodoEventHandler handler) {
		handler.onPublishOnZenodo(this);
	}


}
