package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface DeleteItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 19, 2024
 */
public interface DeleteItemEventHandler extends EventHandler {

	/**
	 * On delete item.
	 *
	 * @param deleteItemEvent the delete item event
	 */
	void onDeleteItem(DeleteItemEvent deleteItemEvent);

}