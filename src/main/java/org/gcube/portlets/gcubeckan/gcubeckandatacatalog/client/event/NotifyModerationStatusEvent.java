package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class NotifyModerationStatusEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 5, 2025
 */
public class NotifyModerationStatusEvent extends GwtEvent<NotifyModerationStatusEventHandler> {

	public static Type<NotifyModerationStatusEventHandler> TYPE = new Type<NotifyModerationStatusEventHandler>();
	private boolean isModerationEnabled = false;

	/**
	 * Instantiates a new notify moderation status event.
	 *
	 * @param isModerationEnabled the is moderation enabled
	 */
	public NotifyModerationStatusEvent(boolean isModerationEnabled) {
		this.isModerationEnabled = isModerationEnabled;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<NotifyModerationStatusEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(NotifyModerationStatusEventHandler handler) {
		handler.onStatus(this);
	}

	/**
	 * Checks if is moderation enabled.
	 *
	 * @return true, if is moderation enabled
	 */
	public boolean isModerationEnabled() {
		return isModerationEnabled;
	}

}
