package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class DeleteItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 19, 2024
 */
public class DeleteItemEvent extends GwtEvent<DeleteItemEventHandler> {
	public static Type<DeleteItemEventHandler> TYPE = new Type<DeleteItemEventHandler>();
	private String itemID;

	/**
	 * Instantiates a new delete item event.
	 *
	 * @param itemID the item ID
	 */
	public DeleteItemEvent(String itemID) {
		this.itemID = itemID;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<DeleteItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(DeleteItemEventHandler handler) {
		handler.onDeleteItem(this);
	}

	/**
	 * Gets the item ID.
	 *
	 * @return the item ID
	 */
	public String getItemID() {
		return itemID;
	}

}
