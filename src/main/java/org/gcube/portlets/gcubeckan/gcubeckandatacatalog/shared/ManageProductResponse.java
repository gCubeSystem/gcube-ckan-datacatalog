package org.gcube.portlets.gcubeckan.gcubeckandatacatalog.shared;

import java.io.Serializable;

/**
 * The Class ManageProductResponse.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 17, 2022
 */
public class ManageProductResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4042294108868246495L;
	// Is GRSF MANAGE in the GRSF context, Is gCat moderation in the other contexts
	private boolean isManageEnabled = false;
	private String scope;

	/**
	 * Instantiates a new manage product response.
	 */
	public ManageProductResponse() {

	}

	/**
	 * Instantiates a new manage product response.
	 *
	 * @param isManageEnabled the is manage enabled
	 * @param scope           the scope
	 */
	public ManageProductResponse(boolean isManageEnabled, String scope) {
		this.isManageEnabled = isManageEnabled;
		this.scope = scope;
	}

	/**
	 * Checks if is manage enabled.
	 *
	 * @return true, if is manage enabled
	 */
	public boolean isManageEnabled() {
		return isManageEnabled;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * Sets the manage enabled.
	 *
	 * @param isManageEnabled the new manage enabled
	 */
	public void setManageEnabled(boolean isManageEnabled) {
		this.isManageEnabled = isManageEnabled;
	}

	/**
	 * Sets the scope.
	 *
	 * @param scope the new scope
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ManageProductResponse [isManageEnabled=");
		builder.append(isManageEnabled);
		builder.append(", scope=");
		builder.append(scope);
		builder.append("]");
		return builder.toString();
	}

}
