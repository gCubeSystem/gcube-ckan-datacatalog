
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.3.5] - 2025-02-05

- Resolved bug "overrides the Visibility" [#28747]
- Enhancement: customized the label/message at the end of workflow publication [#28746]

## [v2.3.4] - 2024-10-15

- Moved to maven-portal-bom 4.0.0{-SNAPSHOT} [#28026]

## [v2.3.3] - 2024-10-01

- Patched `Time_Interval` type via widget [#28111]

## [v2.3.2] - 2024-07-03

- Includes the `metadata-profile-form-builder-widget` enhancement [#27488]

## [v2.3.1] - 2024-05-14

- Includes `ckan-metadata-publisher-widget` with bug fix [#27455] and new feature [#27467]

## [v2.3.0] - 2024-02-02

- Supported the Edit facility [#26639]
- Supported the Delete facility [#26793]
- Revisited the Navigation Bar
- Revisited the "Select Item Resources" step when publishing from Workspace [#26874]
- Catalogue at root VO or VO level works in read only mode [#26854]

## [v2.2.7] - 2023-12-21

- Including "Upload to Zenodo" fix [#26166]

## [v2.2.6] - 2023-07-25

- Just rebuilt the Portlet, fixing issue [#25275]
- Moved to GWT 2.10.0

## [v2.2.5] - 2023-03-02

#### Enhancements

- [#23903] Catalogue Moderation: allow to send a message to the moderators
- [#24309] Inform the VRE users that the catalogue is controlled/moderated 

## [v2.2.4] - 2022-10-27

#### Enhancements

- [#24038] Moved to GWT 2.9
- [#23811] Just to release the grsf-manage-widget


## [v2.2.3] - 2022-08-01

#### Enhancements

- Just to release the optimization implemented for the Moderation Panel [#23692]

## [v2.2.2] - 2022-06-27

#### Enhancements

- Just to release the Publishing Widget enhancement [#23491]
- [#23525] Removed the scope of xml-apis dependency
- Moved to maven-portal-bom v3.6.4

#### Bug fixes

- Just to release the GRSF Manage Widget bug fixes [#23549], [#23561]

## [v2.2.1] - 2022-05-26

#### Enhancements

- [#23406] Including grsf-manage-widget enhancement/bug fixing

## [v2.2.0] - 2022-05-18

#### Enhancements

- [#20650] Data Catalogue: integrate the Content Moderator System
- [#22872] Updated the AppId (to 'service-account-gcat') used in the "Catalogue" GR
- [#23259] Implemented the requirement described in #23156

## [v2.1.1] - 2022-01-21

- Just to release the fix #22691

## [v2.1.0] - 2021-10-05

#### Enhancements

- [#19988] Integrated with `checkEnvironment` to show or not the "Upload to Zenodo" facility


## [v2.0.1] - 2021-05-04

#### Enhancements

- [#21188] Avoiding the catalogue widget window close if the user clicks outside it 
- [#21470] Bug fixed publishing widget uses the orgTitle instead of orgName
- [#20193] Switching to GR "Catalogue" with <AppId>gCat</AppId>. They will be updated by UpdateItemCatalogueResource class. 
- Moved to maven-portal-bom 3.6.3 


## [v2.0.0] - 2021-04-12

#### Enhancements

- [#21153] Upgrade the maven-portal-bom to 3.6.1 version
- [#20699] Migrate the gcube-ckan-datacatalog to catalogue-util-library


## [v1.9.3] - 2021-03-09

- Just to include the ckan2zenodo-library 1.x

## [v1.9.2] - 2021-02-08

#### Bug fixes

- Just to include the fix reported at [#20446]


## [v1.9.1] - 2020-07-08

- Just to include the dependency [#19528]


## [v1.9.0] - 2020-07-03

#### Enhancements

- [#19559] Check and try to avoid the view per VRE configuration for Public and Gateway Catalogue
- Just to include the dependencies [#18700] and [#19479]


## [v1.8.0] - 2019-12-09

- [Feature #18226]: Develop Ckan extension GUI for Zenodo interactive linking


## [v1.7.0] - 2019-12-04

- [Bug #18212]: Guest users are able to access to whole catalogue even if the view per organization is enabled


## [v1.6.1] - 2019-04-17

- [SW Test #16580#note-6]: bug fixing


## [v1.6.0] - 2018-30-08

- [Task #12286]: Align portlet container labels to the new CKAN mapping


## [v1.5.3] - 2018-03-22

- Minor fix


## [v1.5.2] - 2017-10-10

- Minor fix #11210


## [v1.5.2] - 2017-10-10

- Css fix and media rules added for management panels
- Ckan Connector discovering fixed #11094


## [v1.5.1] - 2017-10-10

- Minor improvements
- Removed logout handler
- Added view per VRE (managed with portal custmo fields)


## [v1.5.0] - 2017-05-10

- Metadata model v.3 supported

- Minor fixes


## [v1.4.0] - 2017-04-10

- Added share link facility


## [v1.3.0] - 2017-02-28

- Some optimizations improvements

- Manage button style modified


## [v1.2.1] - 2017-02-01

- Some optimizations improvements


## [v1.2.0] - 2016-12-01

- Groups tab changed
- [Task #5615] Logout is performed before browser tab is closed
- Portlet's borders are removed by default
- Removed asl session


## [v1.1.0] - 2016-10-10

- Version update due to changes at underneath libraries


## [v1.0.0] - 2016-07-12

First release
